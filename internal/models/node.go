package models

import (
	"log"
	"regexp"
)

type INode interface {
	IsRegex() bool
	SetSignature(string)
	GetSignature() string
	SetHelp(string, string)
	GetHelp() (string, string)
	SetFunction(func([]string) (bool, error))
	GetFunction() func([]string) (bool, error)
	SetNode(string, INode)
	GetNode(string) INode
	GetNodes() map[string]INode
}

type node struct {
	regex     bool
	signature string
	help      []string
	function  func([]string) (bool, error)
	nodes     map[string]INode
}

func NewNode() INode {
	return &node{}
}

func (c *node) IsRegex() bool {
	return c.regex
}

func (c *node) SetSignature(n string) {
	rgx, err := regexp.Compile("^<(.+)>$")

	if nil != err {
		log.Fatalln(err.Error())
	}

	res := rgx.FindStringSubmatch(n)

	if 0 != len(res) {
		c.regex = true
		n = n[1 : len(n)-1]
	}

	c.signature = n
}

func (c *node) GetSignature() string {
	return c.signature
}

func (c *node) SetHelp(p string, d string) {
	if nil == c.help {
		c.help = make([]string, 2)
	}

	c.help[0] = p
	c.help[1] = d
}

func (c *node) GetHelp() (string, string) {
	if nil == c.help {
		return "", ""
	}

	return c.help[0], c.help[1]
}

func (c *node) SetFunction(f func([]string) (bool, error)) {
	c.function = f
}

func (c *node) GetFunction() func([]string) (bool, error) {
	return c.function
}

func (c *node) SetNode(k string, v INode) {
	if nil == c.nodes {
		c.nodes = make(map[string]INode)
	}

	c.nodes[k] = v
}

func (c *node) GetNode(k string) INode {
	if v, Ok := c.nodes[k]; Ok {
		return v
	}

	for _, v := range c.nodes {
		if !v.IsRegex() {
			continue
		}

		rgx, err := regexp.Compile(v.GetSignature())
		if nil != err {
			log.Fatalln(err.Error())
		}

		res := rgx.FindStringSubmatch(k)
		if 0 != len(res) {
			return v
		}
	}

	return nil
}

func (c *node) GetNodes() map[string]INode {
	return c.nodes
}
