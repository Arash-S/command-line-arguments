# Go Command Line Argument

## Example

```go
package main

import (
	"fmt"
	"gitlab.com/Arash-S/command-line-arguments/pkg/cmd"
	"os"
)

var command cmd.ICommandLine

func help(a []string) (bool, error) {
	command.PrintHelp()
	return false, nil
}

func testOne(a []string) (bool, error) {
	fmt.Println("Test One function executed")
	fmt.Println(a)
	return false, nil
}

func testTwo(a []string) (bool, error) {
	fmt.Println("Test Two function executed")
	fmt.Println(a)
	return false, nil
}


func testThree(a []string) (bool, error) {
	fmt.Println("Test Three function executed")
	fmt.Println(a)
	return false, nil
}

func init() {
	command = cmd.New()
	
	
	command.Register(cmd.Command{
		"help",
		cmd.Help{
			"help",
			"Display help",
		},
		help,
	})
	
	command.Register(cmd.Command{
		"one",
		cmd.Help{
			"one",
			"this option execute function test one",
		},
		testOne,
	})
	
	command.Register(cmd.Command{
		"one two <^(\\w+)$>",
		cmd.Help{
			"one two <some args>",
			"this option execute function test two with some args",
		},
		testTwo,
	})
	
	command.Register(cmd.Command{
		"one two three",
		cmd.Help{
			"one two three",
			"this option execute function test three",
		},
		testThree,
	})
}

func main() {
	if 1 < len(os.Args) {
		command.Parse(os.Args[1:])
	}
}
```
