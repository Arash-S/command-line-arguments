package cmd

import (
	"fmt"
	"gitlab.com/Arash-S/command-line-arguments/internal/models"
	"log"
	"os"
	"regexp"
	"strings"
	"sync"
	"text/tabwriter"
)

type Help struct {
	Pattern     string
	Description string
}

type Command struct {
	Signature string
	Help      Help
	Function  func([]string) (bool, error)
}

type ICommandLine interface {
	Register(Command)
	Parse([]string)
	PrintHelp()
}

type commandLine struct {
	phrase map[string]models.INode
	help   []string
}

var initializeCommandLineOnce sync.Once
var instanceCommandLine *commandLine

func New() ICommandLine {
	initializeCommandLineOnce.Do(func() {
		instanceCommandLine = &commandLine{
			phrase: make(map[string]models.INode),
			help:   make([]string, 0),
		}
	})

	return instanceCommandLine
}

func (c *commandLine) Register(cmd Command) {
	var prv models.INode
	fld := strings.Split(cmd.Signature, " ")

	if n, ok := c.phrase[fld[0]]; ok {
		prv = n
	} else {
		n := models.NewNode()
		n.SetSignature(fld[0])

		c.phrase[n.GetSignature()] = n
		prv = n
	}

	rgx, err := regexp.Compile("^<(.+)>$")

	if nil != err {
		log.Fatalln(err.Error())
	}

	for i := 1; i < len(fld); i++ {
		var n models.INode
		res := rgx.FindStringSubmatch(fld[i])

		if nil == res {
			n = prv.GetNode(fld[i])
		} else {
			n = prv.GetNode(res[1])
		}

		if nil != n && ((n.IsRegex() && 0 < len(res)) || (!n.IsRegex() && 0 == len(res))) {
			prv = n
			continue
		}

		n = models.NewNode()
		n.SetSignature(fld[i])

		prv.SetNode(n.GetSignature(), n)
		prv = n
	}

	prv.SetHelp(cmd.Help.Pattern, cmd.Help.Description)
	prv.SetFunction(cmd.Function)
}

func (c *commandLine) Parse(a []string) {
	var args []string
	var node models.INode

	c.createHelp(c.phrase)

	if v, ok := c.phrase[a[0]]; ok {
		node = v
	} else {
		c.PrintHelp()
		return
	}

	for _, k := range a[1:] {
		v := node.GetNode(k)

		if nil == v {
			c.PrintHelp()
			return
		}

		if v.IsRegex() {
			args = append(args, k)
		}

		node = v
	}

	f := node.GetFunction()
	if nil == f {
		c.PrintHelp()
		return
	}

	if s, err := f(args); true == s && nil != err {
		log.Fatalln(err.Error())
	}
}

func (c *commandLine) PrintHelp() {
	w := tabwriter.NewWriter(os.Stdout, 0, 8, 1, '\t', tabwriter.AlignRight)
	for _, m := range c.help {
		_, _ = fmt.Fprintf(w, m)
	}

	_ = w.Flush()
}

func (c *commandLine) createHelp(n map[string]models.INode) {

	for _, p := range n {
		if hp, hd := p.GetHelp(); 0 != len(hp) && 0 != len(hd) {
			m := fmt.Sprintf("%s\t%s\n", hp, hd)
			c.help = append(c.help, m)
		}

		if sn := p.GetNodes(); nil != sn {
			c.createHelp(sn)
		}
	}
}
